package com.epam.hibernate.khranovskyi.service.impl;

import com.epam.hibernate.khranovskyi.dto.RoleDto;
import com.epam.hibernate.khranovskyi.dto.UserDto;
import com.epam.hibernate.khranovskyi.entity.Role;
import com.epam.hibernate.khranovskyi.entity.User;
import com.epam.hibernate.khranovskyi.service.RoleAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class RoleAssemblerImpl implements RoleAssembler {
    @Autowired
    private UserAssemblerImpl userAssembler;

    @Override
    public Role assemble(RoleDto dto) {
        Role role = new Role();
        role.setId(dto.getId());
        role.setRole(dto.getRole());

        for (UserDto userDto : dto.getUsers()) {
            User user = assemble(userDto);
            user.setParentRole(role);

            role.getUsers().add(user);
        }

        return role;
    }

    private User assemble(UserDto dto) {
        User user = new User();
        user.setId(dto.getId());
        user.setFirstName(dto.getFirst_name());
        user.setLastName(dto.getLast_name());
        user.setMail(dto.getMail());
        user.setPassword(dto.getPassword());
        user.setActivitiesUsers(dto
                .getActivitiesUsers()
                .stream()
                .map(userAssembler::assemble)
                .collect(Collectors.toList()));

        return user;
    }

    @Override
    public RoleDto assemble(Role entity) {
        RoleDto dto = new RoleDto();
        dto.setId(entity.getId());
        dto.setRole(entity.getRole());

        Set<UserDto> roleDtoSet = entity.getUsers()
                .stream()
                .map(userAssembler::assemble)
                .collect(Collectors.toSet());

        dto.setUsers(roleDtoSet);
        return dto;
    }
}
