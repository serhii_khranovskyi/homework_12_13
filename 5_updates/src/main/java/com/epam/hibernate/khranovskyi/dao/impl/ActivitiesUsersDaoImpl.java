package com.epam.hibernate.khranovskyi.dao.impl;

import com.epam.hibernate.khranovskyi.dao.ActivitiesUsersDAO;
import com.epam.hibernate.khranovskyi.entity.ActivitiesUsers;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.UUID;

@Repository
@Transactional
public class ActivitiesUsersDaoImpl implements ActivitiesUsersDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void save(ActivitiesUsers activityActivitiesUsers) {
        Session session = sessionFactory.getCurrentSession();
        session.save(activityActivitiesUsers);
    }

    @Override
    public ActivitiesUsers get(UUID activityUserId) {
        Session session = sessionFactory.getCurrentSession();
        ActivitiesUsers activitiesUsers = session.get(ActivitiesUsers.class, activityUserId);
        return Objects.requireNonNull(activitiesUsers, "Activity not found by id: " + activityUserId);
    }

    @Override
    public void delete(UUID activityUserId) {
        ActivitiesUsers activitiesUsers = get(activityUserId);
        sessionFactory.getCurrentSession().delete(activitiesUsers);
    }
}
