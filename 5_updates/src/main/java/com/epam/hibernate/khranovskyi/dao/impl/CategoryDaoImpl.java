package com.epam.hibernate.khranovskyi.dao.impl;

import com.epam.hibernate.khranovskyi.dao.CategoryDAO;
import com.epam.hibernate.khranovskyi.entity.Category;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.UUID;

@Repository
@Transactional
public class CategoryDaoImpl implements CategoryDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void delete(UUID categoryId) {
        Category category = get(categoryId);
        sessionFactory.getCurrentSession().delete(category);
    }

    @Override
    public Category get(String name) {
        return (Category) this.sessionFactory.getCurrentSession().createQuery(
                "from Category c where c.categoryName = :name").setParameter("name", name).getSingleResult();
    }

    @Override
    public Category get(UUID categoryId) {
        Session session = sessionFactory.getCurrentSession();
        Category category = session.get(Category.class, categoryId);
        return Objects.requireNonNull(category, "Category not found by id: " + categoryId);
    }

    @Override
    public void save(Category category) {
        Session session = sessionFactory.getCurrentSession();
        session.save(category);
    }
}
