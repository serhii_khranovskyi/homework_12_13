package com.epam.hibernate.khranovskyi.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class UserDto {
    private UUID id = UUID.randomUUID();

    private String mail;

    private String first_name;

    private String last_name;

    private String password;

    private List<ActivitiesUsersDto> activitiesUsers = new ArrayList<>();
}
