package com.epam.hibernate.khranovskyi.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.hibernate.annotations.Cache;

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "roles")
@Getter
@Setter
@NoArgsConstructor
@ToString
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Role {

    @Id
    @Type(type = "uuid-char")
    private UUID id = UUID.randomUUID();

    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @OneToMany(orphanRemoval = true, mappedBy = "parentRole",fetch = FetchType.EAGER)
    private Set<User> users = new HashSet<>();

    @Column(name = "name")
    private String role;
}
