package com.epam.hibernate.khranovskyi.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "languages")
@Getter
@Setter
@NoArgsConstructor
public class Language {

    @Id
    @Type(type = "uuid-char")
    private UUID id = UUID.randomUUID();

    @OneToMany(orphanRemoval = true, mappedBy = "parentLanguage")
    private Set<User> users = new HashSet<>();

    @OneToMany(orphanRemoval = true, mappedBy = "parentCategoryLanguage")
    private Set<CategoryLocalization> categoryLocalizations = new HashSet<>();

    @OneToMany(orphanRemoval = true, mappedBy = "parentActivityLanguage")
    private Set<ActivityLocalization> activityLocalizations = new HashSet<>();

    @Column(name = "name")
    private String languageName;
}
