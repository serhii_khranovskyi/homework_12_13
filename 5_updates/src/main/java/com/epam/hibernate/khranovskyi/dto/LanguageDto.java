package com.epam.hibernate.khranovskyi.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class LanguageDto {
    private UUID id = UUID.randomUUID();

    private Set<UserDto> users = new HashSet<>();

    private Set<CategoryLocalizationDto> categoryLocalizations = new HashSet<>();

    private Set<ActivityLocalizationDto> activityLocalizations = new HashSet<>();

    private String languageName;
}
