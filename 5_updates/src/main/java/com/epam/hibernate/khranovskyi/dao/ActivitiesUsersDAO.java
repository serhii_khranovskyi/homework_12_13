package com.epam.hibernate.khranovskyi.dao;

import com.epam.hibernate.khranovskyi.entity.ActivitiesUsers;

import java.util.UUID;

public interface ActivitiesUsersDAO {
    void save(ActivitiesUsers activitiesUsers);

    ActivitiesUsers get(UUID activityUserId);

    void delete(UUID activityUserId);
}
