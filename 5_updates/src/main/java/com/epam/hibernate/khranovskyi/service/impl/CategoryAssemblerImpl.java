package com.epam.hibernate.khranovskyi.service.impl;

import com.epam.hibernate.khranovskyi.dto.ActivityDto;
import com.epam.hibernate.khranovskyi.dto.CategoryDto;
import com.epam.hibernate.khranovskyi.dto.CategoryLocalizationDto;
import com.epam.hibernate.khranovskyi.entity.Activity;
import com.epam.hibernate.khranovskyi.entity.Category;
import com.epam.hibernate.khranovskyi.entity.CategoryLocalization;
import com.epam.hibernate.khranovskyi.service.CategoryAssembler;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CategoryAssemblerImpl implements CategoryAssembler {
    @Override
    public Category assemble(CategoryDto dto) {
        Category category = new Category();
        category.setId(dto.getId());
        category.setCategoryName(dto.getCategoryName());

        for (ActivityDto activityDto : dto.getActivities()) {
            Activity activity = assemble(activityDto);
            activity.setParentCategory(category);

            category.getActivities().add(activity);
        }

        for (CategoryLocalizationDto categoryLocalizationDto : dto.getCategoryLocalizations()) {
            CategoryLocalization categoryLocalization = assemble(categoryLocalizationDto);
            categoryLocalization.setParentCategoryLocale(category);

            category.getCategoryLocalizations().add(categoryLocalization);
        }

        return category;
    }

    private CategoryLocalization assemble(CategoryLocalizationDto categoryLocalizationDto) {
        CategoryLocalization categoryLocalization = new CategoryLocalization();
        categoryLocalization.setId(categoryLocalizationDto.getId());
        categoryLocalization.setTranslation(categoryLocalizationDto.getTranslation());
        return categoryLocalization;
    }

    private Activity assemble(ActivityDto dto) {
        Activity activity = new Activity();
        activity.setId(dto.getId());
        activity.setActivityName(dto.getActivityName());
        return activity;
    }


    @Override
    public CategoryDto assemble(Category entity) {

        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setId(entity.getId());
        categoryDto.setCategoryName(entity.getCategoryName());


        Set<ActivityDto> categoryDtoSet = entity.getActivities()
                .stream()
                .map(this::assemble)
                .collect(Collectors.toSet());

        categoryDto.setActivities(categoryDtoSet);

        Set<CategoryLocalizationDto> categoryLocalizationDtos = entity.getCategoryLocalizations()
                .stream()
                .map(this::assemble)
                .collect(Collectors.toSet());

        categoryDto.setCategoryLocalizations(categoryLocalizationDtos);
        return categoryDto;
    }

    private CategoryLocalizationDto assemble(CategoryLocalization categoryLocalization) {
        CategoryLocalizationDto categoryLocalizationDto = new CategoryLocalizationDto();
        categoryLocalizationDto.setId(categoryLocalization.getId());
        categoryLocalizationDto.setTranslation(categoryLocalization.getTranslation());
        return categoryLocalizationDto;
    }

    private ActivityDto assemble(Activity activity) {
        ActivityDto activityDto = new ActivityDto();
        activityDto.setId(activity.getId());
        activityDto.setActivityName(activity.getActivityName());
        return activityDto;
    }
}
