package com.epam.hibernate.khranovskyi.service;

import com.epam.hibernate.khranovskyi.dto.RoleDto;
import com.epam.hibernate.khranovskyi.entity.Role;

public interface RoleAssembler {
    Role assemble(RoleDto dto);

    RoleDto assemble(Role entity);
}
