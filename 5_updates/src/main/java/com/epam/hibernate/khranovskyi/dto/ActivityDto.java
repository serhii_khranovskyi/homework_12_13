package com.epam.hibernate.khranovskyi.dto;

import com.epam.hibernate.khranovskyi.entity.ActivitiesUsers;
import com.epam.hibernate.khranovskyi.entity.ActivityLocalization;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.*;

@Getter
@Setter
@NoArgsConstructor
public class ActivityDto {
    private UUID id = UUID.randomUUID();

    private String activityName;

    private Set<ActivityLocalization> activityLocalizations = new HashSet<>();

    private List<ActivitiesUsers> activitiesUsers = new ArrayList<>();
}
