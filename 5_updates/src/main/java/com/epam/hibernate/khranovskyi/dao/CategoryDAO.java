package com.epam.hibernate.khranovskyi.dao;

import com.epam.hibernate.khranovskyi.entity.Category;

import java.util.UUID;

public interface CategoryDAO {
    void save(Category category);

    Category get(UUID categoryId);

    Category get(String name);

    void delete(UUID categoryId);
}