package com.epam.hibernate.khranovskyi.dao.impl;

import com.epam.hibernate.khranovskyi.dao.ActivityLocalizationDAO;
import com.epam.hibernate.khranovskyi.entity.ActivityLocalization;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.UUID;

@Repository
@Transactional
public class ActivityLocalizationDaoImpl implements ActivityLocalizationDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void save(ActivityLocalization activityLocalization) {
        Session session = sessionFactory.getCurrentSession();
        session.save(activityLocalization);
    }

    @Override
    public ActivityLocalization get(UUID activityId) {
        Session session = sessionFactory.getCurrentSession();
        ActivityLocalization activityLocalization = session.get(ActivityLocalization.class, activityId);
        return Objects.requireNonNull(activityLocalization, "ActivityLocalization not found by id: " + activityId);
    }

    @Override
    public ActivityLocalization get(String name) {
        return (ActivityLocalization) this.sessionFactory.getCurrentSession().createQuery(
                "from ActivityLocalization a where a.translation = :name").setParameter("name", name).getSingleResult();

    }

    @Override
    public void delete(UUID activityLocalizationId) {
        ActivityLocalization activityLocalization = get(activityLocalizationId);
        sessionFactory.getCurrentSession().delete(activityLocalization);
    }
}
