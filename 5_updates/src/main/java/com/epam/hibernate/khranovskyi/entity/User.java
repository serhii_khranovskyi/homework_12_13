package com.epam.hibernate.khranovskyi.entity;

import lombok.*;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "users")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class User {

    @Id
    @Column(name = "id_users")
    @Type(type = "uuid-char")
    private UUID id = UUID.randomUUID();


    @Column(name = "mail")
    private String mail;

    @Column(name="first_name")
    private String firstName;

    @Column(name="last_name")
    private String lastName;

    private String password;

    @ManyToOne
    @JoinColumn(name = "role_id")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private Role parentRole;

    @ManyToOne
    @JoinColumn(name = "language_id")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private Language parentLanguage;

    @OneToMany(orphanRemoval = true, mappedBy = "parentUser",fetch = FetchType.EAGER)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<ActivitiesUsers> activitiesUsers = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return mail.equals(user.getMail());
    }
}
