package com.epam.hibernate.khranovskyi.service.impl;

import com.epam.hibernate.khranovskyi.dao.UserDAO;
import com.epam.hibernate.khranovskyi.dto.UserDto;
import com.epam.hibernate.khranovskyi.entity.ActivitiesUsers;
import com.epam.hibernate.khranovskyi.entity.User;
import com.epam.hibernate.khranovskyi.service.UserAssembler;
import com.epam.hibernate.khranovskyi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDAO userDAO;

    @Autowired
    private UserAssembler assembler;

    @Override
    public void delete(UUID id) {
        userDAO.delete(id);
    }

    @Override
    public UserDto create(UserDto dto) {
        User user = assembler.assemble(dto);
        userDAO.save(user);
        return assembler.assemble(user);
    }

    @Override
    public UserDto get(String userName) {
        User user = userDAO.get(userName);
        return assembler.assemble(user);
    }

    @Override
    @Transactional
    public UserDto update(UserDto dto) {
        User user = userDAO.get(dto.getId());
        User updatedEntity = assembler.assemble(dto);

        performUpdate(user, updatedEntity);

        return assembler.assemble(user);
    }

    private void performUpdate(User persistentEntity, User newEntity) {
        persistentEntity.setId(newEntity.getId());
        persistentEntity.setFirstName(newEntity.getFirstName());
        persistentEntity.setLastName(newEntity.getLastName());
        persistentEntity.setMail(newEntity.getMail());
        persistentEntity.setPassword(newEntity.getPassword());
        updateEntity(persistentEntity.getActivitiesUsers(), newEntity.getActivitiesUsers());
    }

    private void updateEntity(List<ActivitiesUsers> persistentActivitiesUsers, List<ActivitiesUsers> newActivitiesUsers) {
        Map<UUID, ActivitiesUsers> stillExistentUsersMap = newActivitiesUsers
                .stream()
                .filter(j -> Objects.nonNull(j.getId()))
                .collect(Collectors.toMap(ActivitiesUsers::getId, Function.identity()));

        List<ActivitiesUsers> journeysToAdd = newActivitiesUsers
                .stream()
                .filter(j -> Objects.isNull(j.getId()))
                .collect(Collectors.toList());

        Iterator<ActivitiesUsers> persistentIterator = persistentActivitiesUsers.iterator();
        while (persistentIterator.hasNext()) {
            ActivitiesUsers persistentActivitiesUser = persistentIterator.next();
            if (stillExistentUsersMap
                    .containsKey(persistentActivitiesUser.getId())) {
                ActivitiesUsers updatedUser = stillExistentUsersMap.get(persistentActivitiesUser.getId());
                persistentActivitiesUser.setParentActivity(updatedUser.getParentActivity());
                persistentActivitiesUser.setParentUser(updatedUser.getParentUser());
            } else {
                persistentIterator.remove();
                persistentActivitiesUser.setParentUser(null);
                persistentActivitiesUser.setParentActivity(null);
            }
        }
        persistentActivitiesUsers.addAll(journeysToAdd);
    }
}
