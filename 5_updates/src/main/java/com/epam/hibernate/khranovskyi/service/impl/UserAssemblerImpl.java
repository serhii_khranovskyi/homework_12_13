package com.epam.hibernate.khranovskyi.service.impl;

import com.epam.hibernate.khranovskyi.dto.ActivitiesUsersDto;
import com.epam.hibernate.khranovskyi.dto.UserDto;
import com.epam.hibernate.khranovskyi.entity.ActivitiesUsers;
import com.epam.hibernate.khranovskyi.entity.User;
import com.epam.hibernate.khranovskyi.service.UserAssembler;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class UserAssemblerImpl implements UserAssembler {
    @Override
    public User assemble(UserDto dto) {
        User user = new User();
        user.setId(dto.getId());
        user.setFirstName(dto.getFirst_name());
        user.setLastName(dto.getLast_name());
        user.setMail(dto.getMail());
        user.setPassword(dto.getPassword());
        user.setActivitiesUsers(dto
                .getActivitiesUsers()
                .stream()
                .map(this::assemble)
                .collect(Collectors.toList()));
        return user;
    }

    public ActivitiesUsers assemble(ActivitiesUsersDto dto) {
        ActivitiesUsers activitiesUsers = new ActivitiesUsers();
        activitiesUsers.setId(dto.getId());
        activitiesUsers.setBeginning(dto.getBeginning());
        activitiesUsers.setEnding(dto.getEnding());
        activitiesUsers.setStatus(dto.getStatus());
        return activitiesUsers;
    }

    @Override
    public UserDto assemble(User user) {
        UserDto dto = new UserDto();
        dto.setId(user.getId());
        dto.setFirst_name(user.getFirstName());
        dto.setLast_name(user.getLastName());
        dto.setMail(user.getMail());
        dto.setPassword(user.getPassword());
        dto.setActivitiesUsers(user
                .getActivitiesUsers()
                .stream()
                .map(this::assemble)
                .collect(Collectors.toList()));
        return dto;
    }

    public ActivitiesUsersDto assemble(ActivitiesUsers entity) {
        ActivitiesUsersDto dto = new ActivitiesUsersDto();
        dto.setId(entity.getId());
        dto.setBeginning(entity.getBeginning());
        dto.setEnding(entity.getEnding());
        dto.setStatus(entity.getStatus());
        return dto;
    }
}
