package com.epam.hibernate.khranovskyi.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "categories_localizations")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CategoryLocalization {
    @Id
    @Type(type = "uuid-char")
    private UUID id = UUID.randomUUID();

    private String translation;

    @ManyToOne
    @JoinColumn(name = "category_id")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private Category parentCategoryLocale;

    @ManyToOne
    @JoinColumn(name = "language_id")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private Category parentCategoryLanguage;
}
