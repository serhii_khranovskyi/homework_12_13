package com.epam.hibernate.khranovskyi.dao.impl;

import com.epam.hibernate.khranovskyi.dao.UserDAO;
import com.epam.hibernate.khranovskyi.entity.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.UUID;

@Repository
@Transactional
public class UserDaoImpl implements UserDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public User get(UUID userId) {
        Session session = sessionFactory.getCurrentSession();
        User user = session.get(User.class, userId);
        return Objects.requireNonNull(user, "Role not found by id: " + userId);
    }

    @Override
    public User get(String mail) {
        return (User) this.sessionFactory.getCurrentSession().createQuery(
                "from User u where u.mail = :mail").setParameter("mail", mail).getSingleResult();
    }

    @Override
    public void delete(UUID userId) {
        User user = get(userId);
        sessionFactory.getCurrentSession().delete(user);
    }

    @Override
    @Transactional
    public void save(User user) {
        Session session = sessionFactory.getCurrentSession();
        session.save(user);
    }
}
