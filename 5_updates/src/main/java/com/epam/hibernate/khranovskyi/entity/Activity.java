package com.epam.hibernate.khranovskyi.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.*;

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "activities")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Activity {

    @Id
    @Type(type = "uuid-char")
    private UUID id = UUID.randomUUID();

    @Column(name = "activity_name")
    private String activityName;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category parentCategory;

    @OneToMany(orphanRemoval = true, mappedBy = "parentActivityLocale")
    private Set<ActivityLocalization> activityLocalizations = new HashSet<>();

    @OneToMany(orphanRemoval = true, mappedBy = "parentActivity")
    private List<ActivitiesUsers> activitiesUsers = new ArrayList<>();
}
