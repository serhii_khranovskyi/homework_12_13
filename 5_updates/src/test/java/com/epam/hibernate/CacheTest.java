package com.epam.hibernate;

import com.epam.hibernate.cfg.HibernateConfiguration;
import com.epam.hibernate.khranovskyi.dao.RoleDAO;
import com.epam.hibernate.khranovskyi.entity.Role;
import net.bytebuddy.utility.RandomString;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.logging.Logger;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfiguration.class})
public class CacheTest {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private RoleDAO roleDAO;

    private static final Logger LOGGER = Logger.getLogger("CacheTest");

    @Test
    public void cacheTest() {
        Role role = new Role();
        role.setRole(RandomString.make(5));
        roleDAO.save(role);
        EntityManager em = sessionFactory.createEntityManager();
        LOGGER.info(em.find(Role.class, role.getId()).toString());

        printCacheInfo();

        em = sessionFactory.createEntityManager();
        LOGGER.info(em.find(Role.class, role.getId()).toString());

    }


    private static void printCacheInfo() {
        List<CacheManager> cacheManagers = CacheManager.ALL_CACHE_MANAGERS;
        if (!cacheManagers.isEmpty()) {
            CacheManager cacheManager = cacheManagers.get(0);
            Cache authorsCache = cacheManager.getCache(Role.class.getName());
            LOGGER.info("Second level cache has size = " + authorsCache.getSize());
        } else {
            LOGGER.info("Hibernate second level cache is disabled.");
        }
    }
}
